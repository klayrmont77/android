import { createRouter, createWebHistory } from '@ionic/vue-router';
import ProduitsView from '../views/ProduitsView.vue'
import BonEntreeView from '../views/BonEntreeView.vue'
import BonSortieView from '../views/BonSortieView.vue'

const routes = [
  {
    path: '/',
    redirect: '/produits'
  },
  {
    path: '/produits',
    name: 'Produits',
    component: ProduitsView
  },
  {
    path: '/bon_entree',
    name: 'BonEntree',
    component: BonEntreeView
  },
  {
    path: '/bon_sortie',
    name: 'BonSortie',
    component: BonSortieView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
