import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'gest_stock_mobapp',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
